import pandas as pd

import numpy as np

dfmay = pd.read_csv('files/file1.csv'
)

dfjune = pd.read_csv('files/file2.csv'
)

#print(dfmay.equals(dfjune))

comparevalues = dfmay.values == dfjune.values

print(comparevalues)

rows,cols = np.where(comparevalues==False)

for item in zip(rows,cols):
    dfmay.iloc[item[0],item[1]] = ' {} --> {} '.format(dfmay.iloc[item[0], item[1]], dfjune.iloc[item[0],item[1]])


dfmay.to_csv('files/output.csv', index=False,header=True)
