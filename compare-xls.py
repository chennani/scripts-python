import pandas as pd

import numpy as np

dffile1 = pd.read_excel('files/file1.xlsx'
)

dffile2 = pd.read_excel('files/file2.xlsx'
)

#print(dffile1.equals(dffile2))

comparevalues = dffile1.values == dffile2.values

print(comparevalues)

rows,cols = np.where(comparevalues==False)

for item in zip(rows,cols):
    dffile1.iloc[item[0],item[1]] = ' {} --> {} '.format(dffile1.iloc[item[0], item[1]], dffile2.iloc[item[0],item[1]])


dffile1.to_excel('files/output.xlsx', index=False,header=True)
